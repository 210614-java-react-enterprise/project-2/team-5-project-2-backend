package dev.team5.api;

public class ApiObject {

    private String link;
    private String description;
    private String title;

    public ApiObject() {super();}

    public ApiObject(String link, String description, String title) {
        this.link = link;
        this.description = description;
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
