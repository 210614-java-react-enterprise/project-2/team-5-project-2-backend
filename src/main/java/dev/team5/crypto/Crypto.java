package dev.team5.crypto;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;

public class Crypto {
    public static String hash(char[] password) {

        SecretKey key = null; //Initialized to make compiler happy

        final String salt = new String("GalladePsychocutNightslashLeafbladeClosecombat");
        //Chosen because it's long, vaguely esoteric, and an absolute monster of a loadout.

        PBEKeySpec hashSpec = new PBEKeySpec(password,salt.getBytes(StandardCharsets.UTF_8),69420,256);
        //256 bits in the DB to store passwords, funny number for number of iterations

        try {
            key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512").generateSecret(hashSpec);
        } catch (Exception e) {
            System.out.println("Could not hash password. Message: ");
            System.out.println(e.getMessage());
        }

        System.out.println(Base64.getEncoder().encodeToString(key.getEncoded()));

        return Base64.getEncoder().encodeToString(key.getEncoded());
    }

    public static void nullifyPassword(char[] password) {
        Arrays.fill(password, '\0');
    }
}

// This file and the password hashing process was implemented by Curtis

