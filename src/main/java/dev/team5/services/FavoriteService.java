package dev.team5.services;

import dev.team5.models.Favorite;
import dev.team5.daos.FavoritesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteService {

    @Autowired
    private FavoritesRepository favoritesRepository;

    public List<Favorite> getFavoritesByAccountId(int accountId) {
        return favoritesRepository.getFavoritesByAccountId(accountId);
    }

    public Favorite addNewFavoriteItem(Favorite favorite) {
        return favoritesRepository.save(favorite);
    }

    public void deleteFavoriteItem(Favorite favorite) {
        favoritesRepository.delete(favorite);
    }

}
