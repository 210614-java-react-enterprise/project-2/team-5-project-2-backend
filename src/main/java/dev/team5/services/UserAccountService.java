package dev.team5.services;

import dev.team5.crypto.Crypto;
import dev.team5.models.UserAccount;
import dev.team5.daos.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAccountService {

    @Autowired
    private UserAccountRepository userAccountRepo;

    @Query("select * from user_accounts")
    public List<UserAccount> getAllUsers() {
        return userAccountRepo.findAll();
    }

    public UserAccount getUserAccountById(int id) {
        return userAccountRepo.getUserAccountByAccountId(id);
    }

    public UserAccount getUserAccountByUsername(String username) {
        return userAccountRepo.getUserAccountByUsername(username);
    }

    public boolean existsUserAccountByUsernameAndPassword(String username, String password) {
        return userAccountRepo.existsUserAccountByUsernameAndUserPass(username, password);
    }

    public UserAccount registerNewUser(UserAccount userAccount) {
        String hashedPassword = Crypto.hash(userAccount.getUserPass().toCharArray());
        userAccount.setUserPass(hashedPassword);

        return userAccountRepo.save(userAccount);
    }


    public void deleteUserAccount(UserAccount userAccount) {
        userAccountRepo.delete(userAccount);
    }



}
