package dev.team5.services;

import dev.team5.daos.ReviewRepository;
import dev.team5.models.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewService {

    @Autowired
    private ReviewRepository reviewRepo;

    @Query("select * from reviews")
    public List<Review> getAllReviews() {
        return reviewRepo.findAll();
    }

    public Review getReviewById(int id) {
        return reviewRepo.getReviewByReviewId(id);
    }

    public List<Review> getReviewByAccountId(int id) {
        return reviewRepo.getReviewByAccountId(id);
    }

    public List<Review> getReviewByProductId(int id) {
        return reviewRepo.getReviewByProductId(id);
    }

    public List<Review> getReviewByProductIdAndScore(int id, int score) {
        return reviewRepo.getReviewByProductIdAndScore(id, score);
    }

    public Review addNewReview(Review review) {
        return reviewRepo.save(review);
    }

    public void deleteReview(Review review) {
        reviewRepo.delete(review);
    }

}
