package dev.team5.services;

import dev.team5.daos.ProductRepository;
import dev.team5.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepo;

    @Query("select * from products")
    public List<Product> getAllProducts() {
        return productRepo.findAll();
    }

    public List<Product> getProductByType(String type) {
        return productRepo.getProductByType(type);
    }

    public List<Product> getProductByNameContainsIgnoreCase(String name) {
        return productRepo.getProductByNameContainsIgnoreCase(name);
    }

    public List<Product> getProductByBrandContainsIgnoreCase(String brand) {
        return productRepo.getProductByBrandContainsIgnoreCase(brand);
    }

    public Product getProductById(int id) {
        return productRepo.getProductByProductId(id);
    }


    public Product addNewProduct(Product product) {
        return productRepo.save(product);
    }

    public void deleteProduct(Product product) {
        productRepo.delete(product);
    }

}
