package dev.team5.services;

import dev.team5.daos.CommentRepository;
import dev.team5.models.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepo;

    @Query("select * from review_comments")
    public List<Comment> getAllComments(){
        return commentRepo.findAll();
    }

    public List<Comment> getCommentByReviewId(int id){
        return commentRepo.findByReviewId(id);
    }

    public List<Comment> getCommentByAccountId(int id){
        return commentRepo.findByAccountId(id);
    }

    public Comment createNewComment(Comment c){
        return commentRepo.save(c);
    }

    public void deleteComment(Comment c){
        commentRepo.delete(c);
    }
}
