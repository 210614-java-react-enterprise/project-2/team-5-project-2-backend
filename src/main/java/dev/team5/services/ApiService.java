package dev.team5.services;

import com.mashape.unirest.http.exceptions.UnirestException;
import dev.team5.api.ApiDriverImpl;
import dev.team5.api.ApiObject;

import java.util.List;

public class ApiService {

    final static ApiDriverImpl api = new ApiDriverImpl();

    public static List<ApiObject> getApiObjects(String string) throws UnirestException {
        return api.requestFromApi(string);

    }
}
