package dev.team5.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;

@Component
@Entity()
@Table(name="review_comments")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "SERIAL")
    private int commentId;

    @Column(name="comment_text")
    private String comment;

    @Column(name="account_id")
    private int accountId;

    @Column(name="review_id")
    private int reviewId;

    public Comment() {
        super();
    }

    public Comment(String comment, int accountId, int reviewId) {
        this.comment = comment;
        this.accountId = accountId;
        this.reviewId = reviewId;
    }

    public Comment(int commentId, String comment, int accountId, int reviewId) {
        this.comment = comment;
        this.commentId = commentId;
        this.accountId = accountId;
        this.reviewId = reviewId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment1 = (Comment) o;
        return commentId == comment1.commentId &&
                accountId == comment1.accountId &&
                reviewId == comment1.reviewId &&
                Objects.equals(comment, comment1.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(comment, commentId, accountId, reviewId);
    }

}
