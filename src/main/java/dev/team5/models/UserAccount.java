package dev.team5.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;

@Component
@Entity()
@Table(name="user_accounts")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class UserAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "SERIAL")
    private int accountId;

    @Column(name = "username")
    private String username;

    @Column(name="user_pass")
    private String userPass;


    public UserAccount() {
    }

    public UserAccount(String username, int accountId, String userPass) {
        this.username = username;
        this.accountId = accountId;
        this.userPass = userPass;
    }

    public UserAccount(String username, String userPass) {
        this.username = username;
        this.userPass = userPass;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return accountId == that.accountId && Objects.equals(username, that.username) && Objects.equals(userPass, that.userPass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, username, userPass);
    }

}
