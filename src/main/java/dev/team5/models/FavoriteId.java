package dev.team5.models;

import java.io.Serializable;
import java.util.Objects;

public class FavoriteId implements Serializable {
    private int accountId;

    private int productId;

    public FavoriteId() {
        super();
    }

    public FavoriteId(int accountId, int productId) {
        this.accountId = accountId;
        this.productId = productId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FavoriteId that = (FavoriteId) o;
        return accountId == that.accountId && productId == that.productId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, productId);
    }

}
