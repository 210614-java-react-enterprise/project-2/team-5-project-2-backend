package dev.team5.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Component
@Entity()
@IdClass(FavoriteId.class)
@Table(name="favorites")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Favorite implements Serializable {

    @Id
    @Column(name = "account_id")
    private int accountId;

    @Id
    @Column(name = "productId")
    private int productId;

    public Favorite() {super();}

    public Favorite(int accountId, int productId) {
        this.accountId = accountId;
        this.productId = productId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Favorite favorite = (Favorite) o;
        return accountId == favorite.accountId && productId == favorite.productId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, productId);
    }


}

