package dev.team5.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;

@Component
@Entity()
@Table(name="reviews")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "SERIAL")
    private int reviewId;

    @Column(name = "score")
    private int score;

    @Column(name = "review_text")
    private String reviewText;

    @Column(name = "account_id")
    private int accountId;

    @Column(name = "product_id")
    private int productId;

    public Review() {
    }

    public Review(int reviewId, String reviewText, int score, int accountId, int productId) {
        this.reviewId = reviewId;
        this.reviewText = reviewText;
        this.score = score;
        this.accountId = accountId;
        this.productId = productId;
    }

    public Review(String reviewText, int score, int accountId, int productId) {
        this.reviewText = reviewText;
        this.score = score;
        this.accountId = accountId;
        this.productId = productId;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public String getReviewText() {
        return reviewText;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }


    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review1 = (Review) o;
        return reviewId == review1.reviewId && score == review1.score && accountId == review1.accountId && productId == review1.productId && Objects.equals(reviewText, review1.reviewText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reviewId, score, reviewText, accountId, productId);
    }


}
