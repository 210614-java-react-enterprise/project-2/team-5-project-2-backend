package dev.team5.controllers;

import dev.team5.models.Favorite;
import dev.team5.services.FavoriteService;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/users/id/{id}/favorites")
public class FavoriteController {

    @Autowired
    FavoriteService favoriteService;

    @GetMapping(produces="application/json")
    public ResponseEntity<List<Favorite>> getFavoritesByAccountId(@PathVariable("id")int id) {
        return new ResponseEntity<>(favoriteService.getFavoritesByAccountId(id), HttpStatus.OK);
    }

    @PostMapping(consumes="application/json")
    public ResponseEntity<Favorite> addNewFavoriteItem(@RequestBody Favorite favorite) {
        Favorite f = favoriteService.addNewFavoriteItem(favorite);
        return new ResponseEntity<>(f, HttpStatus.CREATED);

    }

    @DeleteMapping(consumes = "application/json")
    public ResponseEntity<Favorite> deleteFavoriteItem(@RequestBody Favorite favorite) {
        favoriteService.deleteFavoriteItem(favorite);
        return new ResponseEntity<>(favorite, HttpStatus.ACCEPTED);
    }

}
