package dev.team5.controllers;

import dev.team5.models.Product;
import dev.team5.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(produces="application/json")
    public ResponseEntity<List<Product>> getAllProducts() {
        return new ResponseEntity<List<Product>>(productService.getAllProducts(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id")int id) {
        return new ResponseEntity<>(productService.getProductById(id), HttpStatus.OK);
    }

    @GetMapping("/type/{type}")
    public ResponseEntity<List<Product>> getProductByType(@PathVariable("type")String type) {
        return new ResponseEntity<>(productService.getProductByType(type), HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<List<Product>> getProductByName(@PathVariable("name")String name) {
        return new ResponseEntity<>(productService.getProductByNameContainsIgnoreCase(name), HttpStatus.OK);
    }

    @GetMapping("/brand/{brand}")
    public ResponseEntity<List<Product>> getProductByBrand(@PathVariable("brand")String brand) {
        return new ResponseEntity<>(productService.getProductByBrandContainsIgnoreCase(brand), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<Product> addNewProduct(@RequestBody Product product){
        Product newProduct = productService.addNewProduct(product);
        return new ResponseEntity<>(newProduct, HttpStatus.CREATED);

    }

    @DeleteMapping(consumes = "application/json")
    public ResponseEntity<Product> deleteProduct(@RequestBody Product product){
        productService.deleteProduct(product);
        return new ResponseEntity<>(product, HttpStatus.ACCEPTED);

    }

}
