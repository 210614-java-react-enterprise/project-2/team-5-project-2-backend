package dev.team5.controllers;

import dev.team5.crypto.Crypto;
import dev.team5.models.UserAccount;
import dev.team5.services.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class LoginController {

    @Autowired
    private UserAccountService userAccountService;

    @PostMapping(value="/login", consumes="application/x-www-form-urlencoded")
    public ResponseEntity<String> loginAttempt(@RequestParam("username") String username, @RequestParam("password") String password) {

        String hashedPassword = Crypto.hash(password.toCharArray());
        boolean success = userAccountService.existsUserAccountByUsernameAndPassword(username, hashedPassword);

        if(success) {
            return ResponseEntity.ok().header("Authorization", username+"-login-token").header("Access-Control-Expose-Headers", "*").build();
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }


    @PostMapping(value= "/signup", consumes = "application/json")
    public ResponseEntity<UserAccount> registerNewUser(@RequestBody UserAccount userAccount){
        UserAccount user = userAccountService.registerNewUser(userAccount);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

}
