package dev.team5.controllers;

import dev.team5.models.UserAccount;
import dev.team5.services.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class UserAccountController {

    // For login controller impl., check 7/22 snd vod, starting at 1:52:00

    @Autowired
    private UserAccountService userAccountService;

    @GetMapping(produces="application/json")
    public ResponseEntity<List<UserAccount>> returnAllUsers(){
        return new ResponseEntity<List<UserAccount>>(userAccountService.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<UserAccount> getUserAccountById(@PathVariable("id") int id) {
        return new ResponseEntity<UserAccount>(userAccountService.getUserAccountById(id), HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity<UserAccount> getUserAccountByUsername(@PathVariable("username") String username) {
        return new ResponseEntity<>(userAccountService.getUserAccountByUsername(username), HttpStatus.OK);
    }


    @DeleteMapping(consumes = "application/json")
    public ResponseEntity<UserAccount> deleteUserAccount(@RequestBody UserAccount userAccount){
        userAccountService.deleteUserAccount(userAccount);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

}
