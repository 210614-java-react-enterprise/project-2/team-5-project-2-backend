package dev.team5.controllers;

import com.mashape.unirest.http.exceptions.UnirestException;
import dev.team5.api.ApiObject;
import dev.team5.services.ApiService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class ApiController {

    @GetMapping("/api/{string}")
    public ResponseEntity<List<ApiObject>> returnApiResponse(@PathVariable("string") String string) throws UnirestException {
        return new ResponseEntity<List<ApiObject>>(ApiService.getApiObjects(string), HttpStatus.OK);
    }
}
