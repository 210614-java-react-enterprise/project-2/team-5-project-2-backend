package dev.team5.controllers;

import dev.team5.models.Comment;
import dev.team5.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping(produces="application/json")
    public ResponseEntity<List<Comment>> returnAllComments(){
        return new ResponseEntity<List<Comment>>(commentService.getAllComments(), HttpStatus.OK);
    }

    @GetMapping("/{type}/{id}")
    public ResponseEntity<List<Comment>> returnCommentsById(@PathVariable("id")int id, @PathVariable("type")String type){
        if(type.equals("review")){
            return new ResponseEntity<List<Comment>>(commentService.getCommentByReviewId(id), HttpStatus.OK);
        }else if(type.equals("user")){
            return new ResponseEntity<List<Comment>>(commentService.getCommentByAccountId(id), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<Comment> createNewComment(@RequestBody Comment comment){
        Comment c = commentService.createNewComment(comment);
        return new ResponseEntity<>(c, HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = "application/json")
    public ResponseEntity<Comment> deleteComment(@RequestBody Comment comment){
        commentService.deleteComment(comment);
        return new ResponseEntity<>(comment, HttpStatus.ACCEPTED);
    }

}
