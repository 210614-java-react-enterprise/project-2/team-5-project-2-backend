package dev.team5.controllers;

import dev.team5.models.Review;
import dev.team5.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/reviews")
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @GetMapping(produces="application/json")
    public ResponseEntity<List<Review>> getAllReviews() {
        return new ResponseEntity<List<Review>>(reviewService.getAllReviews(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Review> getReviewById(@PathVariable("id")int id) {
        return new ResponseEntity<>(reviewService.getReviewById(id), HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<List<Review>> getReviewByAccountId(@PathVariable("id")int id) {
        return new ResponseEntity<>(reviewService.getReviewByAccountId(id), HttpStatus.OK);
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<List<Review>> getReviewByProductId(@PathVariable("id")int id) {
        return new ResponseEntity<>(reviewService.getReviewByProductId(id), HttpStatus.OK);
    }

    @GetMapping("/product/{id}/{score}")
    public ResponseEntity<List<Review>> getReviewByProductIdAndScore(@PathVariable("id")int id, @PathVariable("score")int score) {
        return new ResponseEntity<>(reviewService.getReviewByProductIdAndScore(id, score), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<Review> addNewReview(@RequestBody Review review){
        Review rev = reviewService.addNewReview(review);
        return new ResponseEntity<>(rev, HttpStatus.CREATED);

    }

    @DeleteMapping(consumes = "application/json")
    public ResponseEntity<Review> deleteReview(@RequestBody Review review){
        reviewService.deleteReview(review);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }


}
