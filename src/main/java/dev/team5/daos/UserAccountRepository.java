package dev.team5.daos;

import dev.team5.models.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAccountRepository extends JpaRepository<UserAccount, Integer>{
    UserAccount getUserAccountByAccountId(int id);
    UserAccount getUserAccountByUsername(String username);
    boolean existsUserAccountByUsernameAndUserPass(String username, String password);
}
