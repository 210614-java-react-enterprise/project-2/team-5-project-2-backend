package dev.team5.daos;

import dev.team5.models.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Integer> {
    Review getReviewByReviewId(int id);
    List<Review> getReviewByAccountId(int id);
    List<Review> getReviewByProductId(int id);
    List<Review> getReviewByProductIdAndScore(int id, int score);
}
