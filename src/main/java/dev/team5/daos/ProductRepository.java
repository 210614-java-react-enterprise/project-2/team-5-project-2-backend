package dev.team5.daos;

import dev.team5.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    Product getProductByProductId(int id);
    List<Product> getProductByType(String type);
    List<Product> getProductByNameContainsIgnoreCase(String name);
    List<Product> getProductByBrandContainsIgnoreCase(String brand);

}
