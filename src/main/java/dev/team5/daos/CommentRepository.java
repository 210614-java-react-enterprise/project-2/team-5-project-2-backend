package dev.team5.daos;

import dev.team5.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    List<Comment> findByAccountId(int id);
    List<Comment> findByReviewId(int id);
}
