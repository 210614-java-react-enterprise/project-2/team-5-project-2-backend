package dev.team5.daos;

import dev.team5.models.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FavoritesRepository extends JpaRepository<Favorite, Integer> {
    List<Favorite> getFavoritesByAccountId(int accountId);
}
