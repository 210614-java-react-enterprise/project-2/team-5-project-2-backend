package dev.team5.integration;

import dev.team5.models.Review;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ReviewIntegrationTest {

    @Test
    public void getAllReviewsReturnsListAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Review[]> response = restTemplate.exchange(
                "http://localhost:8081/reviews",
                HttpMethod.GET,
                request,
                Review[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void getReviewByIdReturnsSpecificReview() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity<Review> request = new HttpEntity<>(headers);

        ResponseEntity<Review> response = restTemplate.exchange(
                "http://localhost:8081/reviews/11",
                HttpMethod.GET,
                request,
                Review.class);
        Review actualBody = response.getBody();
        Review expectedBody = new Review(11,"It's divine", 5, 13, 1);
        assertAll(
                ()->assertEquals(200, response.getStatusCodeValue()),
                ()->assertEquals(actualBody, expectedBody)
        );
    }

    @Test
    public void getReviewByAccountIdReturnsListForAccount() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Review[]> response = restTemplate.exchange(
                "http://localhost:8081/reviews/user/14",
                HttpMethod.GET,
                request,
                Review[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void getReviewByProductIdReturnsListForProduct() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Review[]> response = restTemplate.exchange(
                "http://localhost:8081/reviews/product/1",
                HttpMethod.GET,
                request,
                Review[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void getReviewByProductIdAndScoreReturnsListMatchingBothParams() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Review[]> response = restTemplate.exchange(
                "http://localhost:8081/reviews/product/4/3",
                HttpMethod.GET,
                request,
                Review[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void addNewReviewReturns201AndNewObject() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject revJson = new JSONObject();
        revJson.put("reviewText", "TestRev");
        revJson.put("score", 3);
        revJson.put("accountId", 14);
        revJson.put("productId", 4);

        HttpEntity<String> request = new HttpEntity<>(revJson.toString(), headers);
        ResponseEntity<Review> response = restTemplate.exchange(
                "http://localhost:8081/reviews",
                HttpMethod.POST,
                request,
                Review.class);
        assertEquals(201, response.getStatusCodeValue());
        assertNotNull(response.getBody());
    }

    @Test
    public void deleteReviewRemovesReviewAndReturns202() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject revJson = new JSONObject();
        revJson.put("reviewText", "TestRev");
        revJson.put("score", 3);
        revJson.put("accountId", 14);
        revJson.put("productId", 4);

        HttpEntity<String> request = new HttpEntity<>(revJson.toString(), headers);
        ResponseEntity<Review> response = restTemplate.exchange(
                "http://localhost:8081/reviews",
                HttpMethod.POST,
                request,
                Review.class);
        Review delReview = response.getBody();

        JSONObject delJson = new JSONObject();
        delJson.put("reviewId", delReview.getReviewId());
        delJson.put("reviewText", "TestRev");
        delJson.put("score", 3);
        delJson.put("accountId", 14);
        delJson.put("productId", 4);

        HttpEntity<String> delRequest = new HttpEntity<>(delJson.toString(), headers);
        ResponseEntity<Review> delResponse = restTemplate.exchange(
                "http://localhost:8081/reviews",
                HttpMethod.DELETE,
                delRequest,
                Review.class);
        assertEquals(202, delResponse.getStatusCodeValue());
    }

}
