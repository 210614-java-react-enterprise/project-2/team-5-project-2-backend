package dev.team5.integration;

import dev.team5.models.Comment;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CommentIntegrationTest {

    @Test
    public void returnAllCommentsGivesListAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Comment[]> response = restTemplate.exchange(
                "http://localhost:8081/comments",
                HttpMethod.GET,
                request,
                Comment[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void returnCommentsByIdForReviewTypeGivesListAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Comment[]> response = restTemplate.exchange(
                "http://localhost:8081/comments/review/10",
                HttpMethod.GET,
                request,
                Comment[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void returnCommentsByIdForUserTypeGivesListAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Comment[]> response = restTemplate.exchange(
                "http://localhost:8081/comments/user/12",
                HttpMethod.GET,
                request,
                Comment[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void returnCommentsByIdForBadPathTypeGives400() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Comment[]> response = restTemplate.exchange(
                "http://localhost:8081/comments/invalid/42",
                HttpMethod.GET,
                request,
                Comment[].class);
        assertEquals(400, response.getStatusCodeValue());
    }

    @Test
    public void createNewCommentReturnsNewObjectAnd201() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject commentJson = new JSONObject();
        commentJson.put("comment", "delete this");
        commentJson.put("accountId", 12);
        commentJson.put("reviewId", 10);

        HttpEntity<String> request = new HttpEntity<>(commentJson.toString(), headers);
        ResponseEntity<Comment> response = restTemplate.exchange(
                "http://localhost:8081/comments",
                HttpMethod.POST,
                request,
                Comment.class);
        Comment delComment = response.getBody();;
        assertNotNull(delComment);
        assertEquals(201, response.getStatusCodeValue());

        JSONObject delJson = new JSONObject();
        delJson.put("commentId", delComment.getCommentId());
        delJson.put("comment", "delete this");
        delJson.put("accountId", 12);
        delJson.put("reviewId", 10);

        HttpEntity<String> delRequest = new HttpEntity<>(delJson.toString(), headers);
        ResponseEntity<Comment> delResponse = restTemplate.exchange(
                "http://localhost:8081/comments",
                HttpMethod.DELETE,
                delRequest,
                Comment.class);
        assertEquals(202, delResponse.getStatusCodeValue());
    }

    @Test
    public void deleteCommentRemovesObjectAndReturns202() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject commentJson = new JSONObject();
        commentJson.put("comment", "delete this");
        commentJson.put("accountId", 12);
        commentJson.put("reviewId", 10);

        HttpEntity<String> request = new HttpEntity<>(commentJson.toString(), headers);
        ResponseEntity<Comment> response = restTemplate.exchange(
                "http://localhost:8081/comments",
                HttpMethod.POST,
                request,
                Comment.class);
        Comment delComment = response.getBody();;
        assertNotNull(delComment);

        JSONObject delJson = new JSONObject();
        delJson.put("commentId", delComment.getCommentId());
        delJson.put("comment", "delete this");
        delJson.put("accountId", 12);
        delJson.put("reviewId", 10);

        HttpEntity<String> delRequest = new HttpEntity<>(delJson.toString(), headers);
        ResponseEntity<Comment> delResponse = restTemplate.exchange(
                "http://localhost:8081/comments",
                HttpMethod.DELETE,
                delRequest,
                Comment.class);
        assertEquals(202, delResponse.getStatusCodeValue());
    }

}
