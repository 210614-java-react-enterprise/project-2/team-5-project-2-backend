package dev.team5.integration;

import dev.team5.models.UserAccount;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class UserAccountIntegrationTest {

    @Test
    public void returnUsersUnauthorizedGives401() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8081/users", String.class);
        assertEquals(401, response.getStatusCodeValue());
    }

    @Test
    public void returnAllUsersGivesListOfUsersAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<UserAccount[]> response = restTemplate.exchange(
                "http://localhost:8081/users",
                HttpMethod.GET,
                request,
                UserAccount[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);

    }

    @Test
    public void getUserAccountByIdReturnsCorrectUser() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity<UserAccount> request = new HttpEntity<>(headers);

        ResponseEntity<UserAccount> response = restTemplate.exchange(
                "http://localhost:8081/users/id/15",
                HttpMethod.GET,
                request,
                UserAccount.class);
        UserAccount actualBody = response.getBody();
        UserAccount expectedBody = new UserAccount( "TestUser14", 15, "pass123");
        assertAll(
                ()->assertEquals(200, response.getStatusCodeValue()),
                ()-> {
                    assert actualBody != null;
                    assertEquals(actualBody.getUsername(), expectedBody.getUsername());
                }
        );
    }

    @Test
    public void getUserAccountByUsernameReturnsCorrectUser() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity<UserAccount> request = new HttpEntity<>(headers);

        ResponseEntity<UserAccount> response = restTemplate.exchange(
                "http://localhost:8081/users/TestUser14",
                HttpMethod.GET,
                request,
                UserAccount.class);
        UserAccount actualBody = response.getBody();
        UserAccount expectedBody = new UserAccount( "TestUser14", 15, "pass123");
        assertAll(
                ()->assertEquals(200, response.getStatusCodeValue()),
                ()-> {
                    assert actualBody != null;
                    assertEquals(actualBody.getUsername(), expectedBody.getUsername());
                }
        );
    }

    @Test
    public void deleteUserAccountRemovesAccountAndGives202() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject userJson = new JSONObject();
        userJson.put("username", "intTest");
        userJson.put("userPass", "intPass");
        HttpEntity<String> request = new HttpEntity<>(userJson.toString(), headers);

        ResponseEntity<UserAccount> dudResponse = restTemplate.exchange(
                "http://localhost:8081/signup",
                HttpMethod.POST,
                request,
                UserAccount.class);
        UserAccount testUser = dudResponse.getBody();
        //Create a new user before removing it, for data integrity

        JSONObject delObj = new JSONObject();
        assert testUser != null;
        delObj.put("accountId", testUser.getAccountId());
        delObj.put("username", "intTest");
        delObj.put("userPass", "intPass");

        HttpEntity<String> delRequest = new HttpEntity<>(delObj.toString(), headers);
        ResponseEntity<UserAccount> response = restTemplate.exchange(
                "http://localhost:8081/users",
                HttpMethod.DELETE,
                delRequest,
                UserAccount.class);
        assertEquals(202, response.getStatusCodeValue());
    }

}
