package dev.team5.integration;

import dev.team5.models.Product;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ProductIntegrationTest {

    @Test
    public void getAllProductsReturnsListOfProductsAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Product[]> response = restTemplate.exchange(
                "http://localhost:8081/products",
                HttpMethod.GET,
                request,
                Product[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void getProductByIdReturnsCorrectProduct() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity<Product> request = new HttpEntity<>(headers);

        ResponseEntity<Product> response = restTemplate.exchange(
                "http://localhost:8081/products/1",
                HttpMethod.GET,
                request,
                Product.class);
        Product actualBody = response.getBody();
        Product expectedBody = new Product(1, "Spiced Rum", "Liquor", "Captain Morgan");
        assertAll(
                ()->assertEquals(200, response.getStatusCodeValue()),
                ()->assertEquals(actualBody, expectedBody)
        );
    }

    @Test
    public void getProductByTypeReturnsListAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Product[]> response = restTemplate.exchange(
                "http://localhost:8081/products/type/Liquor",
                HttpMethod.GET,
                request,
                Product[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void getProductByNameReturnsListByNameAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Product[]> response = restTemplate.exchange(
                "http://localhost:8081/products/name/Rum",
                HttpMethod.GET,
                request,
                Product[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void getProductsByBrandReturnsListByBrandAnd200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Product[]> response = restTemplate.exchange(
                "http://localhost:8081/products/brand/Captain",
                HttpMethod.GET,
                request,
                Product[].class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void addNewProductCreatesNewObjectReturns201() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject productJson = new JSONObject();
        productJson.put("name", "Water");
        productJson.put("type", "Non-Alcoholic");
        productJson.put("brand", "Liquid Death");

        HttpEntity<String> request = new HttpEntity<>(productJson.toString(), headers);
        ResponseEntity<Product> response = restTemplate.exchange(
                "http://localhost:8081/products",
                HttpMethod.POST,
                request,
                Product.class);
        Product actualBody = response.getBody();
        Product expectedBody = new Product("Water", "Non-Alcoholic", "Liquid Death");
        assertAll(
                ()->assertEquals(201,response.getStatusCodeValue()),
                ()-> {
                    assert actualBody != null;
                    assertEquals(actualBody.getName(), expectedBody.getName());
                    assertEquals(actualBody.getBrand(), expectedBody.getBrand());
                }
        );
    }

    @Test
    public void deleteProductRemovesProductReturns202() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject productJson = new JSONObject();
        productJson.put("name", "Water");
        productJson.put("type", "Non-Alcoholic");
        productJson.put("brand", "Liquid Death");

        HttpEntity<String> request = new HttpEntity<>(productJson.toString(), headers);
        ResponseEntity<Product> response = restTemplate.exchange(
                "http://localhost:8081/products",
                HttpMethod.POST,
                request,
                Product.class);
        Product delProduct = response.getBody();
        assert delProduct != null;

        JSONObject delJson = new JSONObject();
        delJson.put("productId", delProduct.getProductId());
        delJson.put("name", "Water");
        delJson.put("type", "Non-Alcoholic");
        delJson.put("brand", "Liquid Death");

        HttpEntity<String> delRequest = new HttpEntity<>(delJson.toString(), headers);
        ResponseEntity<Product> delResponse = restTemplate.exchange(
                "http://localhost:8081/products",
                HttpMethod.DELETE,
                delRequest,
                Product.class);
        assertEquals(202, delResponse.getStatusCodeValue());
    }

}
