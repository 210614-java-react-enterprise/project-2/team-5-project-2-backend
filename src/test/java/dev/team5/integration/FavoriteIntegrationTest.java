package dev.team5.integration;

import dev.team5.models.Favorite;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class FavoriteIntegrationTest {

    @Test
    public void getFavoritesByAccountIdReturnsListForAccount() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<Favorite[]> response = restTemplate.exchange(
                "http://localhost:8081/users/id/15/favorites",
                HttpMethod.GET,
                request,
                Favorite[].class);

        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response);
    }

    @Test
    public void addNewFavoriteItemReturnsNewItem() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject favJson = new JSONObject();
        favJson.put("accountId", 15);
        favJson.put("productId", 4);

        HttpEntity<String> request = new HttpEntity<>(favJson.toString(), headers);
        ResponseEntity<Favorite> response = restTemplate.exchange(
                "http://localhost:8081/users/id/15/favorites",
                HttpMethod.POST,
                request,
                Favorite.class);

        assertEquals(201, response.getStatusCodeValue());
        assertNotNull(response);

        JSONObject delJson = new JSONObject();
        delJson.put("accountId", 15);
        delJson.put("productId", 4);

        HttpEntity<String> delRequest = new HttpEntity<>(delJson.toString(), headers);
        ResponseEntity<Favorite> delResponse = restTemplate.exchange(
                "http://localhost:8081/users/id/15/favorites",
                HttpMethod.DELETE,
                delRequest,
                Favorite.class);
        assertEquals(202, delResponse.getStatusCodeValue());
    }

    @Test
    public void deleteFavoriteItemRemovesItemAndReturns202() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth");
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject favJson = new JSONObject();
        favJson.put("accountId", 15);
        favJson.put("productId", 4);

        HttpEntity<String> request = new HttpEntity<>(favJson.toString(), headers);
        ResponseEntity<Favorite> response = restTemplate.exchange(
                "http://localhost:8081/users/id/15/favorites",
                HttpMethod.POST,
                request,
                Favorite.class);
        assertEquals(201, response.getStatusCodeValue());

        JSONObject delJson = new JSONObject();
        delJson.put("accountId", 15);
        delJson.put("productId", 4);

        HttpEntity<String> delRequest = new HttpEntity<>(delJson.toString(), headers);
        ResponseEntity<Favorite> delResponse = restTemplate.exchange(
                "http://localhost:8081/users/id/15/favorites",
                HttpMethod.DELETE,
                delRequest,
                Favorite.class);
        assertEquals(202, delResponse.getStatusCodeValue());
    }

}
