package dev.team5.integration;

import dev.team5.models.UserAccount;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class LoginIntegrationTest {

    @Test
    public void badLoginAttemptReturns401() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
        request.add("username", "invalid");
        request.add("password", "invalid");
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(request, headers);

        ResponseEntity<String> response =
                restTemplate.exchange("http://localhost:8081/login",
                        HttpMethod.POST,
                        requestEntity,
                        String.class);

        assertEquals(401, response.getStatusCodeValue());
    }

    @Test
    public void validLoginAttemptReturns200() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
        request.add("username", "TestUser14");
        request.add("password", "pass123");
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(request, headers);

        ResponseEntity<String> response =
                restTemplate.exchange("http://localhost:8081/login",
                        HttpMethod.POST,
                        requestEntity,
                        String.class);
        assertEquals(200, response.getStatusCodeValue());

    }

    @Test
    public void registerNewUserReturns201AndObject() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject userJson = new JSONObject();
        userJson.put("username", "intTest");
        userJson.put("userPass", "intPass");
        HttpEntity<String> request = new HttpEntity<>(userJson.toString(), headers);

        ResponseEntity<UserAccount> response = restTemplate.exchange(
                "http://localhost:8081/signup",
                HttpMethod.POST,
                request,
                UserAccount.class);
        UserAccount actualBody = response.getBody();
        UserAccount expectedBody = new UserAccount("intTest", "intPass");
        assertAll(
                ()->assertEquals(201,response.getStatusCodeValue()),
                ()-> {
                    assert actualBody != null;
                    assertEquals(actualBody.getUsername(), expectedBody.getUsername());
                }
        );
    }

}
