package dev.team5.unit;

import dev.team5.controllers.ReviewController;
import dev.team5.models.Review;
import dev.team5.services.ReviewService;
import net.bytebuddy.utility.visitor.ExceptionTableSensitiveMethodVisitor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ReviewControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ReviewService reviewService;

    @Autowired
    ReviewController reviewController;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(reviewController).build();
    }

    @Test
    public void authAspectGives401OnRequestsWithNoAuthHeader() throws Exception {
        this.mockMvc.perform(get("/reviews"))
                .andExpect(status().is(401));
    }

    @Test
    public void getAllReviewsReturnsListOfReviews() throws Exception {
        List<Review> listReview = Arrays.asList(
                            new Review("revText", 5, 99, 101),
                            new Review("revText", 4, 98, 102),
                            new Review("revText", 3, 97, 103));
        doReturn(listReview).when(reviewService).getAllReviews();

        this.mockMvc.perform(get("/reviews").header("Authorization", "auth"))
                    .andExpect(status().is(200))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].accountId").value(hasItems(99, 98, 97)));

    }

    @Test
    public void getReviewByIdReturnsCorrectReview() throws Exception {
        Review testRev = new Review(66, "testRevId", 4, 99, 101 );
        doReturn(testRev).when(reviewService).getReviewById(66);

        this.mockMvc.perform(get("/reviews/66").header("Authorization", "auth"))
                    .andExpect(status().is(200))
                    .andExpect(MockMvcResultMatchers.content()
                    .string("{\"reviewId\":66,\"score\":4,\"reviewText\":\"testRevId\",\"accountId\":99,\"productId\":101}"));

    }

    @Test
    public void getReviewByAccountIdReturnsListOfReviewsByUser() throws Exception {
        List<Review> usersRevs = Arrays.asList(
                new Review(66, "testRevId", 4, 99, 101 ),
                new Review(67, "testRevId", 4, 99, 103 ));
        doReturn(usersRevs).when(reviewService).getReviewByAccountId(99);

        this.mockMvc.perform(get("/reviews/user/99").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].reviewId").value(hasItems(66, 67)));

    }

    @Test
    public void getReviewByProductIdReturnsListOfProductReviews() throws Exception {
        List<Review> usersRevs = Arrays.asList(
                new Review(67, "testRevId", 4, 99, 103 ),
                new Review(68, "testRevId", 3, 99, 103 ));
        doReturn(usersRevs).when(reviewService).getReviewByProductId(103);

        this.mockMvc.perform(get("/reviews/product/103").header("Authorization", "auth"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].reviewId").value(hasItems(67, 68)));


    }

    @Test
    public void getReviewByProductIdAndScoreReturnsReviewsForProductWithCorrectScore() throws Exception {
        List<Review> usersRevs = Arrays.asList(
                new Review(67, "testRevId", 4, 99, 103 ),
                new Review(68, "testRevId", 4, 99, 103 ));
        doReturn(usersRevs).when(reviewService).getReviewByProductIdAndScore(103, 4);

        this.mockMvc.perform(get("/reviews/product/103/4").header("Authorization", "auth"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].reviewId").value(hasItems(67, 68)));

    }

    @Test
    public void addNewReviewReturns400WhenEmpty() throws Exception {
        Review emptyRev = new Review();
        doReturn(null).when(reviewService).addNewReview(emptyRev);

        this.mockMvc.perform(post("/reviews").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("")).andExpect(status().isBadRequest());

    }

    @Test
    public void addNewReviewReturnsNewReviewWhenValid() throws Exception {
        Review reviewNew = new Review("revText", 3, 14, 4);
        doReturn(reviewNew).when(reviewService).addNewReview(reviewNew);

        this.mockMvc.perform(post("/reviews").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"score\": 3,\"reviewText\": \"TestRev\",\"accountId\": 14,\"productId\": 4}"))
                    .andExpect(status().isCreated());
    }

    @Test
    public void deleteReviewReturnsAcceptedResponseCode() throws Exception {
        Review delRev = new Review();
        doNothing().when(reviewService).deleteReview(delRev);

        this.mockMvc.perform(delete("/reviews").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"reviewId\":\"66\" ,\"score\": 3,\"reviewText\": \"TestRev\",\"accountId\": 14,\"productId\": 4}"))
                    .andExpect(status().is(202));

    }

}
