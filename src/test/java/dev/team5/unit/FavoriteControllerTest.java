package dev.team5.unit;

import dev.team5.models.Favorite;
import dev.team5.services.FavoriteService;
import dev.team5.controllers.FavoriteController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class FavoriteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    FavoriteService favoriteService;

    @Autowired
    FavoriteController favoriteController;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(favoriteController).build();
    }

    //Start testing here
    @Test
    public void getFavoritesByAccountIdReturnsListMatchingId() throws Exception {
        List<Favorite> listId = Arrays.asList(
                new Favorite(15, 7),
                new Favorite(15, 8),
                new Favorite(15, 12));
        doReturn(listId).when(favoriteService).getFavoritesByAccountId(15);

        this.mockMvc.perform(get("/users/id/15/favorites").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].productId").value(hasItems(7,8,12)));
    }

    @Test
    public void addNewFavoriteItemReturns400IfEmpty() throws Exception {
        Favorite newFavorite = new Favorite();
        doReturn(null).when(favoriteService).addNewFavoriteItem(newFavorite);

        this.mockMvc.perform(post("/users/id/15/favorites").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(""))
                    .andExpect(status().is(400));
    }

    @Test
    public void addNewFavoriteItemReturns201AndItemWhenValid() throws Exception {
        Favorite newFavorite = new Favorite(99,66);
        doReturn(newFavorite).when(favoriteService).addNewFavoriteItem(newFavorite);

        this.mockMvc.perform(post("/users/id/99/favorites").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"accountId\":99,\"productId\":66}"))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteFavoriteItemReturns202() throws Exception {
        Favorite delFavorite = new Favorite(99,66);
        doNothing().when(favoriteService).deleteFavoriteItem(delFavorite);

        this.mockMvc.perform(delete("/users/id/99/favorites").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"accountId\":99,\"productId\":66}"))
                    .andExpect(status().is(202));
    }

}
