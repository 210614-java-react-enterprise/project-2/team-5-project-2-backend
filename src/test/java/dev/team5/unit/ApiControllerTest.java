package dev.team5.unit;

import dev.team5.controllers.ApiController;
import dev.team5.services.ApiService;
import dev.team5.api.ApiObject;
import dev.team5.api.ApiDriverImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ApiControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ApiService apiService;

    @MockBean
    ApiDriverImpl apiDriver;

    @Autowired
    ApiController apiController;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(apiController).build();
    }

    @Test
    public void returnApiResponseGivesListOfApiObjects() throws Exception {
        ApiObject setObj = new ApiObject();
        setObj.setLink("apiLink2");
        setObj.setDescription("apiDesc");
        setObj.setTitle("setTitle");
        List<ApiObject> apiObjects = Arrays.asList(
                new ApiObject("apiLink1", "apiDesc", "apiTitle"),
                setObj,
                new ApiObject("apiLink3", "apiDesc", "apiTitle"));

        this.mockMvc.perform(get("/api/q=captain+morgan&num=3").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

}
