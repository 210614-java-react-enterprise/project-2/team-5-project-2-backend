package dev.team5.unit;

import dev.team5.controllers.UserAccountController;
import dev.team5.models.UserAccount;
import dev.team5.services.UserAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UserAccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserAccountService userAccountService;

    @Autowired
    UserAccountController userAccountController;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(userAccountController).build();
    }

    @Test
    public void returnAllUsersReturnsList() throws Exception{

    List<UserAccount> accountList = Arrays.asList(
            new UserAccount("SpringTest1", "passTest"),
            new UserAccount("SpringTest2", "passTest"),
            new UserAccount("SpringTest3", "passTest"));

    doReturn(accountList).when(userAccountService).getAllUsers();

    this.mockMvc.perform(get("/users").header("Authorization" , "auth"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].username").value(hasItems("SpringTest1","SpringTest2","SpringTest3")));

    }

    @Test
    public void getUserAccountByIdReturnsCorrectUser() throws Exception {
        UserAccount testUser = new UserAccount("testUser", 99, "testPass");
        doReturn(testUser).when(userAccountService).getUserAccountById(99);

        this.mockMvc.perform(get("/users/id/99").header("Authorization" , "auth"))
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content()
                    .string("{\"accountId\":99,\"username\":\"testUser\",\"userPass\":\"testPass\"}"));

    }

    @Test
    public void getUserAccountByUsernameReturnsCorrectUser() throws Exception {
        UserAccount testUser = new UserAccount("testUser", 500, "testPass");
        doReturn(testUser).when(userAccountService).getUserAccountByUsername(testUser.getUsername());

        this.mockMvc.perform(get("/users/testUser").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content()
                    .string("{\"accountId\":500,\"username\":\"testUser\",\"userPass\":\"testPass\"}"));

    }

    @Test
    public void deleteUserAccountRemovesUserOnlyIfNoFKRelationships() throws Exception {
        UserAccount testUser = new UserAccount("testUser", 99, "testPass");
        doNothing().when(userAccountService).deleteUserAccount(testUser);

        this.mockMvc.perform(delete("/users").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"accountId\": 99, \"username\": \"testUser\",\"userPass\": \"testPass\"}"))
                    .andExpect(status().is(202))
                    .andExpect(MockMvcResultMatchers.content().string(""));

    }

}
