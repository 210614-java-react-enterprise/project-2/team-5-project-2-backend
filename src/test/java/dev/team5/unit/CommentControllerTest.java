package dev.team5.unit;

import dev.team5.models.Comment;
import dev.team5.services.CommentService;
import dev.team5.controllers.CommentController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class CommentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CommentService commentService;

    @Autowired
    CommentController commentController;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(commentController).build();
    }

    @Test
    public void returnAllCommentsReturnsListOfComments() throws Exception {
        List<Comment> commentList = Arrays.asList(
                new Comment(50, "commentText", 15, 66),
                new Comment(51, "commentText", 15, 66),
                new Comment(52, "commentText", 15, 66));
        doReturn(commentList).when(commentService).getAllComments();

        this.mockMvc.perform(get("/comments").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].commentId").value(hasItems(50,51,52)));
    }

    @Test
    public void returnCommentsByReviewIdReturnsCommentListForReview() throws Exception {
        List<Comment> reviewComments = Arrays.asList(
                new Comment(99, "comment", 15, 20),
                new Comment(98, "comment", 15, 20));
        doReturn(reviewComments).when(commentService).getCommentByReviewId(20);

        this.mockMvc.perform(get("/comments/review/20").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].commentId").value(hasItems(99,98)));
    }

    @Test
    public void returnCommentsByAccountIdReturnsListByAccount() throws Exception {
        List<Comment> accountComments = Arrays.asList(
                new Comment(99, "comment", 15, 20),
                new Comment(98, "comment", 15, 20));
        doReturn(accountComments).when(commentService).getCommentByAccountId(15);

        this.mockMvc.perform(get("/comments/user/15").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].commentId").value(hasItems(99,98)));
    }

    @Test
    public void returnCommentsByIdGives400OnBadPath() throws Exception {
        this.mockMvc.perform(get("/comments/invalid/15").header("Authorization", "auth"))
                    .andExpect(status().is(400));
    }

    @Test
    public void createNewCommentReturns201AndNewObject() throws Exception {
        Comment newComment = new Comment("newComment", 15, 20);
        doReturn(newComment).when(commentService).createNewComment(newComment);

        this.mockMvc.perform(post("/comments").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"comment\":\"newComment\",\"accountId\":15,\"reviewId\":20}"))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteCommentReturns202AndTheDeletedObject() throws Exception {
        Comment delComment = new Comment(66, "comment", 15, 20);
        doNothing().when(commentService).deleteComment(delComment);

        this.mockMvc.perform(delete("/comments").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"commentId\":66,\"comment\":\"comment\",\"accountId\":15,\"reviewId\":20}"))
                    .andExpect(status().is(202))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

}
