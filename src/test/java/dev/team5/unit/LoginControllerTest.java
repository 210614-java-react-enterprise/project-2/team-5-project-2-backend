package dev.team5.unit;

import dev.team5.controllers.LoginController;
import dev.team5.crypto.Crypto;
import dev.team5.models.UserAccount;
import dev.team5.services.UserAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class LoginControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserAccountService userAccountService;

    @Autowired
    LoginController loginController;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(loginController).build();
    }

    @Test
    public void loginAttemptAcceptsValidCredentials() throws Exception {
        UserAccount testUser = new UserAccount("TestUserSpring", 999, "JUnitPass");

        String hashedPassword = Crypto.hash(testUser.getUserPass().toCharArray());
        doReturn(true).when(userAccountService)
                .existsUserAccountByUsernameAndPassword(testUser.getUsername(), hashedPassword);

        this.mockMvc.perform(post("/login")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    .param("username", "TestUserSpring")
                    .param("password", "JUnitPass"))
                    .andExpect(status().is(200));
    }

    @Test
    public void loginAttemptRejectInvalidCredentials() throws Exception {
        this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("username", "Invalid")
                .param("password", "nopass"))
                .andExpect(status().is(401));
    }

    @Test
    public void signupCreatesNewUser() throws Exception {
        UserAccount signupTest = new UserAccount("newUserTest", "newUserPass");
        doReturn(signupTest).when(userAccountService).registerNewUser(signupTest);

        this.mockMvc.perform(post("/signup")
                    .contentType(MediaType.APPLICATION_JSON)
                    .characterEncoding("utf-8")
                    .content("{\"username\":\"newUserTest\", \"userPass\":\"newUserPass\"}"))
                    .andExpect(status().isCreated());
    }


}
