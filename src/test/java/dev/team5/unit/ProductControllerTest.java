package dev.team5.unit;

import dev.team5.models.Product;
import dev.team5.services.ProductService;
import dev.team5.controllers.ProductController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ProductService productService;

    @Autowired
    ProductController productController;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    public void getAllProductsReturnsListOfProducts() throws Exception {
        List<Product> productList = Arrays.asList(
                        new Product(99, "Rum", "Liquor", "Captain Morgan"),
                        new Product(100, "Rum", "Liquor", "Captain Morgan"),
                        new Product(101, "Rum", "Liquor", "Captain Morgan"));
        doReturn(productList).when(productService).getAllProducts();

        this.mockMvc.perform(get("/products").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].productId").value(hasItems(99, 100, 101)));

    }

    @Test
    public void getProductByIdReturnsOneProduct() throws Exception {
        Product product = new Product(99, "Water", "Non-Alcoholic", "Dasani");
        doReturn(product).when(productService).getProductById(99);

        this.mockMvc.perform(get("/products/99").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content()
                    .string("{\"productId\":99,\"name\":\"Water\",\"type\":\"Non-Alcoholic\",\"brand\":\"Dasani\",\"imageUrl\":null}"));

    }

    @Test
    public void getProductByTypeReturnsListByType() throws Exception {
        List<Product> typeList = Arrays.asList(
                            new Product(66, "TestN", "TestT", "TestB"),
                            new Product(67, "TestN", "TestT", "TestB"),
                            new Product(68, "TestN", "TestT", "TestB"));
        doReturn(typeList).when(productService).getProductByType("TestT");

        this.mockMvc.perform(get("/products/type/TestT").header("Authorization", "auth"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].productId").value(hasItems(66, 67,68)));
    }

    @Test
    public void getProductByNameReturnsListByName() throws Exception {
        List<Product> nameList = Arrays.asList(
                new Product(66, "TestN", "TestT", "TestB"),
                new Product(67, "TestN", "TestT", "TestB"),
                new Product(68, "TestN", "TestT", "TestB"));
        doReturn(nameList).when(productService).getProductByNameContainsIgnoreCase("TestN");

        this.mockMvc.perform(get("/products/name/TestN").header("Authorization", "auth"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].productId").value(hasItems(66, 67,68)));
    }

    @Test
    public void getProductByBrandReturnsListByBrand() throws Exception {
        List<Product> brandList = Arrays.asList(
                new Product(66, "TestN", "TestT", "TestB"),
                new Product(67, "TestN", "TestT", "TestB"),
                new Product(68, "TestN", "TestT", "TestB"));
        doReturn(brandList).when(productService).getProductByBrandContainsIgnoreCase("TestB");

        this.mockMvc.perform(get("/products/brand/TestB").header("Authorization", "auth"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].productId").value(hasItems(66, 67,68)));
    }

    @Test
    public void addNewProductReturnsNewObjectAnd201() throws Exception {
        Product newProduct = new Product("nameTest", "typeTest", "brandTest");
        doReturn(newProduct).when(productService).addNewProduct(newProduct);

        this.mockMvc.perform(post("/products").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"name\":\"nameTest\",\"type\":\"typeTest\",\"brand\":\"brandTest\"}"))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void addNewProductReturns400WhenEmpty() throws Exception {
        Product emptyProduct = null;
        doReturn(null).when(productService).addNewProduct(emptyProduct);

        this.mockMvc.perform(post("/products").header("Authorization","auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(""))
                    .andExpect(status().is(400));
    }

    @Test
    public void deleteProductReturns400WhenEmpty() throws Exception {
        Product emptyProduct = new Product();
        doNothing().when(productService).deleteProduct(emptyProduct);

        this.mockMvc.perform(delete("/products").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(""))
                    .andExpect(status().is(400));
    }

    @Test
    public void deleteProductReturns202WhenNotEmpty() throws Exception {
        Product delProduct = new Product(99, "DeleteMe", "Deletable", "Dell");
        doNothing().when(productService).deleteProduct(delProduct);

        this.mockMvc.perform(delete("/products").header("Authorization", "auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"productId\":99,\"name\":\"DeleteMe\",\"type\":\"Deletable\",\"brand\":\"Dell\"}"))
                    .andExpect(status().is(202));
    }

}
