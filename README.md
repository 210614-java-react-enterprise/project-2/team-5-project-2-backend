Repository for the front-end web files can be found here: https://gitlab.com/210614-java-react-enterprise/project-2/team5-pj2-frontend

# Project 2-Swig (Greg McCoy, Nick Bilotta, and Curtis Greene-Morgan)

For this project our group chose to create a web application called Swig. Swig is a web app that would allow users to make a review on any drink they'd like. Alcoholic, Non-Alcoholic it does not matter. If a user likes a Review, they'll be happy to know that there is a way to favorite reviews. Users can even create comments for reviews and delete thier comments. Swig also uses a google search api to find whatever product is being mentioned in the review, and gives you 3 links to choose to learn more or just purchase that product. Users are also able to delete their own reviews if they'd like.

## High-Level Requirements

Application leverages the full stack: 
- RDBMS for persistence 
- API built with Java 8 and Spring
- UI built with HTML, CSS, Bootstrap, and JS

Technology frameworks: 
- Hibernate to communicate with a PostGreSQL RDBMS 
- The Spring Framework 
- RESTful
- Complete CI/CD pipelines will use AWS (EC2 with Jenkins, or CodePipeline, CodeBuild, Elastic Beanstalk, and S3)
- Google Search ApI 
- AWS RDS 
- AWS EC2 
- AWS S3 
- >=80% test coverage for service layer and have testing reports using Jacoco
- Spring's MockMvc for integration/e2e tests of controller endpoints
- Java Docs and web endpoint documentation [Swagger/OpenAPI]
